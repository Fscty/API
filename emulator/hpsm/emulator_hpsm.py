from flask import Flask, jsonify, request, render_template
from datetime import datetime
import json, requests

app = Flask(__name__)
app.json_provider_class.compact = False

@app.route('/', methods=['GET'])
def index():
    return render_template('index.html')

@app.route('/', methods=['POST'])
def process_data():
    url = "http://10.5.1.50:14930/SM/9/rest/incidents"
    urgency = request.form.get('Urgency')
    title = request.form.get('Title')
    description = request.form.get('Description')
    openedBy = request.form.get('OpenedBy')
    ticketOwner = request.form.get('TicketOwner')
    
    incident = {
        "Urgency": urgency,
        "Title": title,
        "Description": [description],
        "OpenedBy": openedBy,
        "TicketOwner": ticketOwner,
    }

    response = requests.post(url, json=incident)
    return render_template('index.html')



@app.route('/SM/9/rest/incidents', methods=['GET'])
def get_incidents():
    if 'status' in request.args:
        open_status = request.args.get("status")
        status_list = []
        with open('incidents.json', 'r') as file:
            incidents_data = json.load(file)

        for incident in incidents_data:
            if incidents_data[incident]["Status"] == open_status:
                incident_data = {
                    "IncidentID": incident,
                    "Status": open_status
                }
                status_list.append(incident_data)
        return jsonify(status_list)
    
    else:
        with open('incidents.json', 'r') as file:
            incidents_data = json.load(file)

        incidents_list = []
        incident_ids = list(incidents_data.keys())

        for incident in incident_ids:
            incident_data = {"IncidentID": incident}
            incidents_list.append(f'Incident:{incident_data}')

        response = {
            "@totalcount": len(incidents_list),
            "@start": 1,
            "@count": len(incidents_list),
            "Messages": [],
            "content": incidents_list,
            "ReturnCode": 0,
            "ResourceName": "Incident"
        }   


        return jsonify(response)   


@app.route('/SM/9/rest/incidents/<id>', methods=['GET'])
def get_incident(id):
    with open('incidents.json', 'r') as file:
        incidents_data = json.load(file)
    if id in list(incidents_data.keys()):
        incident_info = {
            id: {
                "IncidentID": incidents_data[id]["IncidentID"],
                "Status": incidents_data[id]["Status"],
                "Assignee": incidents_data[id]["Assignee"],
                "Urgency": incidents_data[id]["Urgency"],
                "OpenTime": incidents_data[id]["OpenTime"],
                "ClosedTime": incidents_data[id]["ClosedTime"],
                "Title": incidents_data[id]["Title"],
                "Description": incidents_data[id]["Description"],
                "Solution": incidents_data[id]["Solution"],
                "OpenedBy": incidents_data[id]["OpenedBy"],
                "ClosedBy": incidents_data[id]["ClosedBy"],
                "TicketOwner": incidents_data[id]["TicketOwner"],
                "AssignmentGroup": incidents_data[id]["AssignmentGroup"],
                "UpdatedBy": incidents_data[id]["UpdatedBy"],
                "UpdatedTime": incidents_data[id]["UpdatedTime"]
            }
        }
        return jsonify(incident_info)
    else:
        return "", 404
    
    
@app.route('/SM/9/rest/incidents', methods=['POST'])
def create_incidents():
    with open('incidents.json', 'r') as file:
        incidents_data = json.load(file)
    incidents = list(incidents_data.keys())
    i = [incident for incident in incidents]
    last_number = i[-1]
    last_number_num = int(last_number[2:])
    next_number = f"IM{last_number_num + 1:05d}" 
    json_data = request.json
    new_incident = {
        next_number: {
            "IncidentID": next_number,
            "Status": "Open",
            "Assignee":"",
            "Urgency": json_data["Urgency"],
            "OpenTime": datetime.now().isoformat().split(".")[0],
            "ClosedTime": "",
            "Title": json_data["Title"],
            "Description": json_data["Description"],
            "Solution":[],
            "OpenedBy": json_data["OpenedBy"],
            "ClosedBy":"",
            "TicketOwner": json_data["TicketOwner"],
            "AssignmentGroup":"",
            "UpdatedBy":"",
            "UpdatedTime":"",
        }
    }
    
    with open('incidents.json', 'r') as file:
        json_data = json.load(file)
        json_data.update(new_incident)
        

    with open('incidents.json', 'w') as file:
        json.dump(json_data, file)
        

    return jsonify(json_data)


app.run(host='0.0.0.0', port=14930)