import time, requests
from datetime import datetime

url = "http://10.5.1.50:14930/SM/9/rest/incidents"


def poll_tickets():
    create_time = {}
    last_check_time = datetime.now().isoformat().split(".")[0]
     
    while True:
        response = requests.get(url).json()
        content = response['content']
        incident_ids = [item.split("'")[3] for item in content]
        for inc in incident_ids:
            create_time[inc] = requests.get(f'{url}/{inc}').json()[inc]['OpenTime']
        current_time = datetime.now().isoformat().split(".")[0]
        for i in create_time:
            if create_time[i] > last_check_time:
                print(f"Создана новая заявка - {i} : {create_time[i]}")
        last_check_time = current_time
        time.sleep(30)

poll_tickets()