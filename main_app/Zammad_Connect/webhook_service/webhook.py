from flask import Flask, jsonify, request
from flask_basicauth import BasicAuth
from dotenv import load_dotenv
import os, base64


app = Flask(__name__)
basic_auth = BasicAuth(app)

load_dotenv()

username = os.getenv('USERNAME')
password = os.getenv('PASSWORD')
encoded_credentials = base64.b64encode(f'{username}:{password}'.encode("utf-8")).decode("utf-8")


app.config['BASIC_AUTH_USERNAME'] = username
app.config['BASIC_AUTH_PASSWORD'] = password



@app.route('/webhook/data', methods=['POST'])
@basic_auth.required
def handle_webhook():
    data = request.json
    host = request.headers.get('Host')
    user_agent = request.headers.get('User-Agent')
    trigger = request.headers.get('X-Zammad-Trigger')
    auth = request.headers.get('Authorization')
    if host == "10.5.1.50:5000" and user_agent == "Zammad User Agent" and trigger == "test2" and auth == f'Basic {encoded_credentials}':
        print(data)
        return jsonify(data)
    else:
        return "", 404
    




context = ('server.crt', 'server.key') 
app.run(host='0.0.0.0', port=5000, ssl_context=context)

    
